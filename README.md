# 概要
#### Web上で記事の作成・編集・削除ができる日記保存サービス
---
---
## 機能一覧
* 記事投稿
* 記事一覧表示
* 記事詳細表示
* 記事編集
* 記事削除
* 管理ユーザー登録
* 管理ユーザログイン
* 画像ファイルのアップロード
* ページネーション
* テスト
---
## 使用している技術の一覧
* 言語: Python3
* フレームワーク: Django
* ソース管理: Git/Bitbucket
* CSS: Bootstrap
* 承認機能: django-allauth
* 画像アップロード: Pillow
* テスト: selenium
* インフラ: AWS_EC2 / Amazon Linux2
* Webサーバー: Nginx
* データベース: PostgreSQL
